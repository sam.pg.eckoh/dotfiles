For gnome-keyring, make following alterations to PAM files:  
_/etc/pam.d/login_  

_**at end of auth section**_  
auth    optional    pam_gnome_keyring.section  

_**at end of session section**_  
session    optional    pam_gnome_keyring.so auto_start  

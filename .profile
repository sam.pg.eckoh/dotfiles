case "$DESKTOP_SESSION" in
	i3)
		export $(gnome-keyring-daemon --start)
		;;
esac
## This requires the following modifications to PAM files
## /etc/pam.d/login
## at end of auth section:
## auth       optional     pam_gnome_keyring.so
##
## at end of session section:
## session    optional     pam_gnome_keyring.so auto_start
## 
## changes to /etc/pam.d/passwd are not required

# switch off annoying puppet warnings
export RUBYOPT='-W0'

